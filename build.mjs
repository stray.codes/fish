import {context} from 'esbuild'

const loaderConfig = {
	".glsl": "text",
	".gltf": "json",
	".png": "dataurl",
	".obj": "text"
}

const options = {
	entryPoints: [ "index.js" ],
	outfile: "index.bundle.js",
	loader: loaderConfig,
	bundle: true,
	format: "esm",
	sourcemap: true
}

const ctx = await context(options);

await ctx.watch();

# Animated fish website background

A lightweight, dynamic and responsive fish background for websites powered by JavaScript and WebGL.
You can see how it looks in use [here](https://cv.stray.codes/).
Color, texture and the size of the fish can be changed.
I will add more detailed instructions and a documentation in the future.

## Compile instructions

This project is dependend on ES-Build. 
Clone, install dependencies and build with: 
```
git clone https://gitlab.com/stray.codes/fish.git
cd fish
npm install esbuild
npm run autoBuild 
```

You can setup a temporary http server for testing purpouses with:
```
python3 -m http.server
```
## How it works
The individual fish are [boids](https://en.wikipedia.org/wiki/Boids).
"Boids is an artificial life program, developed by Craig Reynolds in 1986, which simulates the flocking behaviour of birds. His paper on this topic was published in 1987 in the proceedings of the ACM SIGGRAPH conference." (Source: Wikipedia)

All the calculations are executed within a shader. The positions, velocity, direction and other data written and stored in a texture. Shaders in general execute calculations in parallel, which is well suited for this task.

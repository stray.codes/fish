//returns various 3x3 matrices
export var m3 = {
    identity: function () {
        return [
            1, 0, 0,
            0, 1, 0,
            0, 0, 1
        ];
    },
    translation: function (tx, ty) {
        return [
            1, 0, 0,
            0, 1, 0,
            0, 0, 1,
            tx, ty, 1
        ];
    }

}

//Create a gird of vertices, so that a texture could be drawn on.
//The aspect ratio is 1:2
export var positionGrid = {
    generate: function (detail) {
        let i_detail = parseInt(detail);
        //check if detail is larger than two and an even number
        if (i_detail < 2 || i_detail % 2 == 1) {
            throw "ERROR: detail is either smaller than 2 or not even!";
        }
        function getSquare(initialPosition, width, orientation) {
            return getTrianglesFromSquare([[initialPosition[0], initialPosition[1]],
            [initialPosition[0], initialPosition[1] + width],
            [initialPosition[0] + width, initialPosition[1]],
            [initialPosition[0] + width, initialPosition[1] + width]], orientation);
        }
        function getTrianglesFromSquare(squarePositions, orientation) {
            if (orientation == 1) {
                return [
                    squarePositions[0],
                    squarePositions[1],
                    squarePositions[2],
                    squarePositions[2],
                    squarePositions[1],
                    squarePositions[3]
                ];
            }
            else if (orientation == -1) {
                return [
                    squarePositions[0],
                    squarePositions[3],
                    squarePositions[2],
                    squarePositions[0],
                    squarePositions[1],
                    squarePositions[3]
                ];
            }
            else
                throw "ERROR: wrong orientation";
        }

        let size = 1.0 / parseFloat(i_detail);
        let positions = [];
        let currentOrientation = 1;
        for (let jj = 0; jj < i_detail * 2; jj++) {
            for (let ii = 0; ii < i_detail / 2; ii++) {
                positions.push(...getSquare([ii * size, jj * size], size, currentOrientation));
                positions.push(...getSquare([(ii + (i_detail / 2)) * size, jj * size], size, currentOrientation));
                currentOrientation = currentOrientation * -1;
            }
            currentOrientation = currentOrientation * -1;
        }
        let positionsFormated = [];
        for (let ii = 0; ii < positions.length; ii++) {
            positionsFormated.push(positions[ii][0]);
            positionsFormated.push(positions[ii][1]);
        }

        return new Float32Array(positionsFormated);
    }
}

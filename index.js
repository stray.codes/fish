//import {loadImage, loadFile, loadJson} from './loader.js'
import {GLProgramData} from './gl/glShaders.js'
import {bufferData, uniformInt, uniformFloat, uniformVec2, uniformVec3, loadTexture} from './gl/glData.js'
//import * as glUtils from 'gl/glTextures.js'
import {positionGrid} from './math.js'

import v_grid from './shader/v_grid.glsl'
import f_grid from './shader/f_grid.glsl'
import v_texture from './shader/v_texture.glsl'
import f_texture from './shader/f_texture.glsl'
import v_swarm from './shader/v_swarm.glsl'
import f_swarm from './shader/f_swarm.glsl'
import textureSource from './textures/alpha.png'


//colors
let color0 = "#E69EE4"
let color1 = "#B8399E"
let color2 = "#B8399E"
let color3 = "#E69EE4"
let color4 = "#E69EE4"
let color5 = "#E69EE4"


//variables and constants
let detail = 3;
let fishUnit = 55;
let radiusFishSeparation = 1.5;
let radiusFishCohesion = 3;
let radiusFishAllignment = 2.5;
let radiusMouse1 = 2;

let friction = 0.75;
let maxVelocity = 2000;
let maxAcceleration = 2000;
let maxAccelerationDiff = 2000;

let scalarMouse = -1700;
let scalarSeparation = -350;
let scalarAllignment = 150;
let scalarCohesion = 30;
let scalarFood = 600;


let position;
let mousePosition = [-1000.0, -1000.0];
let mouseMode = 0;
let mouseModeDuration = 10.0;
let mouseModeTime = mouseModeDuration;

let debug = false;
let showFish = true;
let showGrid = debug;
let lockRedraw = false;
let debugFrequency = 1.0;


let currentDataTex = 1;
let dataTextureWidth = 6;



const canvas = document.querySelector('#c');

canvas.width = window.innerWidth;
canvas.height = window.innerHeight;
const gl = canvas.getContext('webgl2');
if (!gl) {
    console.log("Failed to get context. No webGL2!");
}
gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
window.addEventListener('resize', function () {//TODO make sure its executed in the right time
    lockRedraw = true;
    canvas.width = window.innerWidth;
    canvas.height = window.innerHeight;
    gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
    nextAmount = calculateAmount();
    if (nextAmount > maxAmount)
        nextAmount = maxAmount;
    if (nextAmount > amount)
        spawnDespawnTimer = spawnTimerDuration;
    if (nextAmount < amount)
        spawnDespawnTimer = despawnTimerDuration;
    programData_swarm.use();
    uniformInt(programData_swarm.data(), "u_nextAmount", nextAmount);
    if (programData_grid != null && programData_swarm != null && programData_texture != null)
        updateResolution();
    lockRedraw = false;
    //requestAnimationFrame(drawScene);
});


window.addEventListener("mousemove", (event) => {
    mousePosition = [event.clientX, event.clientY];
    updateMouse(mousePosition);
});

window.addEventListener("message", (event) => {
    // extract the data from the message event
    const {data} = event;

    if (data.x != null && data.y != null) {
        mousePosition = [data.x, data.y];
        updateMouse(mousePosition);
    }
    if (data.color0 != null && color1 != null && color2 != null && color3 != null && color4 != null && color5 != null)
    {
        updateColors(data);
    }
    if (data.size != null){
        fishUnit=data.size;
        programData_swarm.use();
        uniformFloat(programData_swarm.data(), "u_fishUnit", fishUnit);
        programData_texture.use();
        uniformInt(programData_texture.data(), "u_size", fishUnit);
    }
});

function updateColors(data) {
    color0 = data.color0;
    color1 = data.color1;
    color2 = data.color2;
    color3 = data.color3;
    color4 = data.color4;
    color5 = data.color5;

    programData_texture.use();
    uniformVec3(programData_texture.data(), "u_color0", hexToRgb(color0));
    uniformVec3(programData_texture.data(), "u_color1", hexToRgb(color1));
    uniformVec3(programData_texture.data(), "u_color2", hexToRgb(color2));
    uniformVec3(programData_texture.data(), "u_color3", hexToRgb(color3));
    uniformVec3(programData_texture.data(), "u_color4", hexToRgb(color4));
    uniformVec3(programData_texture.data(), "u_color5", hexToRgb(color5));
}

function updateMouse(mousePosition) {
    programData_swarm.use();
    uniformVec2(programData_swarm.data(), "u_mouse", mousePosition);
    mouseModeTime = mouseModeDuration;
    if (mouseMode != 0) {
        mouseMode = 0;
        uniformInt(programData_swarm.data(), "u_mouseMode", mouseMode);
    }
}

let amount = calculateAmount();
let maxAmount = 1024;
if (amount > maxAmount)
    amount = maxAmount;
let nextAmount = amount;
let spawnDespawnTimer = 0;
let despawnTimerDuration = 5;
let spawnTimerDuration = 0.7;


//create and compile shaders and shader programs
let programData_swarm = new GLProgramData(gl, v_swarm, f_swarm);
let programData_texture = new GLProgramData(gl, v_texture, f_texture);
let programData_grid = new GLProgramData(gl, v_grid, f_grid);


//create all textures
let texture_swarmData0 = loadTexture(programData_swarm, 0, null); //read
let texture_swarmData1 = loadTexture(programData_swarm, 1, null); //write-read
loadTexture(programData_texture, 2, textureSource);

//load all data
programData_swarm.use();
uniformVec2(programData_swarm.data(), "u_mouse", mousePosition);
uniformVec2(programData_swarm.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
uniformInt(programData_swarm.data(), "u_amount", amount);
uniformInt(programData_swarm.data(), "u_nextAmount", nextAmount);

uniformFloat(programData_swarm.data(), "u_fishUnit", fishUnit);
uniformFloat(programData_swarm.data(), "u_radiusFishSeparation", radiusFishSeparation);
uniformFloat(programData_swarm.data(), "u_radiusFishCohesion", radiusFishCohesion);
uniformFloat(programData_swarm.data(), "u_radiusFishAllignment", radiusFishAllignment);
uniformFloat(programData_swarm.data(), "u_radiusMouse1", radiusMouse1);

uniformFloat(programData_swarm.data(), "u_friction", friction);
uniformFloat(programData_swarm.data(), "u_maxVelocity", maxVelocity);
uniformFloat(programData_swarm.data(), "u_maxAcceleration", maxAcceleration);
uniformFloat(programData_swarm.data(), "u_maxAccelerationDiff", maxAccelerationDiff);

uniformFloat(programData_swarm.data(), "u_scalarMouse", scalarMouse);
uniformFloat(programData_swarm.data(), "u_scalarSeparation", scalarSeparation);
uniformFloat(programData_swarm.data(), "u_scalarAllignment", scalarAllignment);
uniformFloat(programData_swarm.data(), "u_scalarCohesion", scalarCohesion);
uniformFloat(programData_swarm.data(), "u_scalarFood", scalarFood);

uniformInt(programData_swarm.data(), "u_mouseMode", mouseMode);


programData_texture.use();
position = positionGrid.generate(Math.pow(2, detail));
bufferData(programData_texture.data(), 2, gl.FLOAT, "a_position", position);
uniformVec2(programData_texture.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
uniformInt(programData_texture.data(), "u_size", fishUnit);
uniformFloat(programData_texture.data(), "u_maxAcceleration", maxAcceleration);
uniformFloat(programData_texture.data(), "u_maxAccelerationDiff", maxAccelerationDiff);
gl.uniform1i(gl.getUniformLocation(programData_texture.program, "u_texture"), 2);

const hexToRgb = hex =>
    hex.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i
        , (m, r, g, b) => '#' + r + r + g + g + b + b)
        .substring(1).match(/.{2}/g)
        .map(x => parseInt(x, 16) / 255)


uniformVec3(programData_texture.data(), "u_color0", hexToRgb(color0));
uniformVec3(programData_texture.data(), "u_color1", hexToRgb(color1));
uniformVec3(programData_texture.data(), "u_color2", hexToRgb(color2));
uniformVec3(programData_texture.data(), "u_color3", hexToRgb(color3));
uniformVec3(programData_texture.data(), "u_color4", hexToRgb(color4));
uniformVec3(programData_texture.data(), "u_color5", hexToRgb(color5));


if (showGrid) {
    programData_grid.use();
    bufferData(programData_grid.data(), 2, gl.FLOAT, "a_position", position);
    uniformVec2(programData_grid.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
    uniformInt(programData_grid.data(), "u_size", fishUnit);
    uniformFloat(programData_grid.data(), "u_maxAcceleration", maxAcceleration);
    uniformFloat(programData_grid.data(), "u_maxAccelerationDiff", maxAccelerationDiff);
}

const fb_swarm = gl.createFramebuffer();


var uniformLocation_delta_texture = gl.getUniformLocation(programData_texture.program, "u_delta");
var uniformLocation_time_texture = gl.getUniformLocation(programData_texture.program, "u_time");
var uniformLocation_fishID_texture = gl.getUniformLocation(programData_texture.program, "u_fishID");
var uniformLocation_dataTexture_texture = gl.getUniformLocation(programData_texture.program, "u_dataTexture")
var uniformLocation_dataTexture_swarm = gl.getUniformLocation(programData_swarm.program, "u_dataTexture")


//init
programData_swarm.use();
flipDataTexture();
textureFramebuffer(maxAmount);
programData_swarm.use();
uniformInt(programData_swarm.data(), "u_amount", maxAmount);
uniformInt(programData_swarm.data(), "u_init", 1);
uniformFloat(programData_swarm.data(), "u_delta", 1.0);
gl.drawArrays(gl.POINTS, 0, dataTextureWidth * maxAmount);
uniformInt(programData_swarm.data(), "u_init", 0);
uniformInt(programData_swarm.data(), "u_amount", amount);
if (debug) {
    var data = new Int32Array(dataTextureWidth * maxAmount * 4);
    gl.readPixels(0, 0, dataTextureWidth, maxAmount, gl.RGBA_INTEGER, gl.INT, data);
    console.log(data);
}





//game loop
var time_old = 0.0;
var delta = 0.0;
var debugTimer = debugFrequency;

gl.enable(gl.DEPTH_TEST);
//gl.enable(gl.CULL_FACE);

requestAnimationFrame(drawScene);
function drawScene(time) {
    if (!lockRedraw) {
        delta = (time - time_old) / 1000;
        time_old = time;
        mouseModeTime = mouseModeTime - delta;
        if (mouseModeTime <= 0 && mouseMode != 1) {
            mouseMode = 1;
            programData_swarm.use();
            uniformInt(programData_swarm.data(), "u_mouseMode", mouseMode);
        }

        if (spawnDespawnTimer > 0.0) {
            spawnDespawnTimer = spawnDespawnTimer - delta;
            if (spawnDespawnTimer <= 0.0) {
                spawnDespawnTimer = -1;
                amount = nextAmount;
                programData_swarm.use();
                uniformInt(programData_swarm.data(), "u_amount", amount);
                programData_texture.use();
                uniformInt(programData_texture.data(), "u_amount", nextAmount);
            }
        }


        flipDataTexture();
        programData_swarm.use();
        textureFramebuffer(amount);
        draw_swarm(delta);

        debugTimer = debugTimer - delta;
        if (debug && debugTimer <= 0.0) {
            debugTimer = debugFrequency;
            console.log("FPS: " + 1 / delta);
            console.log("TIME: " + time);
            console.log("MouseMode: " + mouseMode);
            var data = new Int32Array(dataTextureWidth * amount * 4);
            gl.readPixels(0, 0, dataTextureWidth, amount, gl.RGBA_INTEGER, gl.INT, data);
            console.log("DATA TEXTURE: ");
            console.log(data);
        }

        gl.bindFramebuffer(gl.FRAMEBUFFER, null);
        gl.viewport(0, 0, gl.canvas.width, gl.canvas.height);
        gl.clear(gl.COLOR_BUFFER_BIT | gl.DEPTH_BUFFER_BIT);
        if (showFish) {
            programData_texture.use();
            gl.uniform1f(uniformLocation_delta_texture, delta);
            gl.uniform1f(uniformLocation_time_texture, time);
            for (var i = 0; i < amount; i++) {
                gl.uniform1i(uniformLocation_fishID_texture, i);
                gl.drawArrays(gl.TRIANGLES, 0, position.length / 2);
            }
        }
        if (showGrid) {

            programData_grid.use();

            uniformFloat(programData_grid.data(), "u_delta", delta);
            uniformFloat(programData_grid.data(), "u_time", time);
            for (var i = 0; i < amount; i++) {
                uniformInt(programData_grid.data(), "u_fishID", i);
                for (let ii = 0; ii < position.length / 2; ii = ii + 3) {
                    gl.drawArrays(gl.LINE_LOOP, ii, 3);
                }
            }

        }

        requestAnimationFrame(drawScene);
    }
}

function flipDataTexture() {
    currentDataTex = currentDataTex * -1;
    if (currentDataTex == 1) {
        programData_swarm.use();
        gl.uniform1i(uniformLocation_dataTexture_swarm, 0);
        programData_texture.use();
        gl.uniform1i(uniformLocation_dataTexture_texture, 1);
    }
    else {
        programData_swarm.use();
        gl.uniform1i(uniformLocation_dataTexture_swarm, 1);
        programData_texture.use();
        gl.uniform1i(uniformLocation_dataTexture_texture, 0);
    }
}

function textureFramebuffer(renderAmount) {
    gl.bindFramebuffer(gl.FRAMEBUFFER, fb_swarm);
    gl.viewport(0, 0, dataTextureWidth, renderAmount);
    if (currentDataTex == 1) {
        gl.activeTexture(gl.TEXTURE0 + 1);
        gl.bindTexture(gl.TEXTURE_2D, texture_swarmData1);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture_swarmData1, 0);
    }
    else {
        gl.activeTexture(gl.TEXTURE0 + 0);
        gl.bindTexture(gl.TEXTURE_2D, texture_swarmData0);
        gl.framebufferTexture2D(gl.FRAMEBUFFER, gl.COLOR_ATTACHMENT0, gl.TEXTURE_2D, texture_swarmData0, 0);
    }
}



function draw_swarm(delta) {
    programData_swarm.use();
    uniformFloat(programData_swarm.data(), "u_delta", delta);
    gl.drawArrays(gl.POINTS, 0, dataTextureWidth * amount);
}




function calculateAmount() {
    return parseInt((gl.canvas.width * gl.canvas.height / (100 * 100 * 2)) * 0.6);
    //return parseInt((gl.canvas.width * gl.canvas.height / (fishUnit * fishUnit * 2)) * 0.6);
}

function updateResolution() {
    programData_texture.use();
    uniformVec2(programData_texture.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
    programData_grid.use();
    uniformVec2(programData_grid.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
    programData_swarm.use();
    uniformVec2(programData_swarm.data(), "u_resolution", [gl.canvas.width, gl.canvas.height]);
}


export function bufferData(glProgramData, size, type, name, data) {
    var attribLocation = glProgramData.gl.getAttribLocation(glProgramData.program, name);
    var buffer = glProgramData.gl.createBuffer();
    glProgramData.gl.bindBuffer(glProgramData.gl.ARRAY_BUFFER, buffer);
    glProgramData.gl.bufferData(glProgramData.gl.ARRAY_BUFFER, data, glProgramData.gl.STATIC_DRAW);
    glProgramData.gl.enableVertexAttribArray(attribLocation);
    //var type = glProgramData.gl.FLOAT;
    var normalize = false;
    var stride = 0;
    var offset = 0;
    glProgramData.gl.vertexAttribPointer(attribLocation, size, type, normalize, stride, offset);
}



export function uniformFloat(glProgramData, name, data) {
    var uniformLocation = glProgramData.gl.getUniformLocation(glProgramData.program, name);
    glProgramData.gl.uniform1f(uniformLocation, data);
}
export function uniformInt(glProgramData, name, data) {
    var uniformLocation = glProgramData.gl.getUniformLocation(glProgramData.program, name);
    glProgramData.gl.uniform1i(uniformLocation, data);
}

export function uniformVec2(glProgramData, name, data) {
    var uniformLocation = glProgramData.gl.getUniformLocation(glProgramData.program, name);
    glProgramData.gl.uniform2fv(uniformLocation, new Float32Array(data));
}

export function uniformVec3(glProgramData, name, data) {
    var uniformLocation = glProgramData.gl.getUniformLocation(glProgramData.program, name);
    glProgramData.gl.uniform3fv(uniformLocation, new Float32Array(data));
}

export function loadTexture(glProgramData, index, textureSource) {
    glProgramData.gl.pixelStorei(glProgramData.gl.UNPACK_COLORSPACE_CONVERSION_WEBGL, false);
    glProgramData.gl.pixelStorei(glProgramData.gl.UNPACK_ALIGNMENT, 1);
    glProgramData.gl.activeTexture(glProgramData.gl.TEXTURE0 + index);
    var texture = glProgramData.gl.createTexture();
    glProgramData.gl.bindTexture(glProgramData.gl.TEXTURE_2D, texture);
    glProgramData.gl.texParameteri(glProgramData.gl.TEXTURE_2D, glProgramData.gl.TEXTURE_WRAP_S, glProgramData.gl.CLAMP_TO_EDGE);
    glProgramData.gl.texParameteri(glProgramData.gl.TEXTURE_2D, glProgramData.gl.TEXTURE_WRAP_T, glProgramData.gl.CLAMP_TO_EDGE);
    glProgramData.gl.texParameteri(glProgramData.gl.TEXTURE_2D, glProgramData.gl.TEXTURE_MIN_FILTER, glProgramData.gl.NEAREST);
    glProgramData.gl.texParameteri(glProgramData.gl.TEXTURE_2D, glProgramData.gl.TEXTURE_MAG_FILTER, glProgramData.gl.NEAREST);

    if (textureSource == null) {
        // fill texture with 1x1 pixels
        const level = 0;
        const internalFormat = glProgramData.gl.RGBA32I;
        const width = 6;
        const height = 1024;
        const border = 0;
        const format = glProgramData.gl.RGBA_INTEGER;
        const type = glProgramData.gl.INT;
        //const data = new Int32Array([
            //-830, 5, 6, 30,
            //122, 343, 3, 255 
        //]);
        glProgramData.gl.activeTexture(glProgramData.gl.TEXTURE0 + index);
        glProgramData.gl.texImage2D(glProgramData.gl.TEXTURE_2D, level, internalFormat, width, height, border, format, type, null);
    }
    else {
        let textureImage = new Image;
        textureImage.src = textureSource;
        textureImage.onload = () => {
            glProgramData.gl.activeTexture(glProgramData.gl.TEXTURE0 + index);
            glProgramData.gl.texImage2D(glProgramData.gl.TEXTURE_2D, 0, glProgramData.gl.RGBA, glProgramData.gl.RGBA, glProgramData.gl.UNSIGNED_BYTE, textureImage);
            //var imageLocation = glProgramData.gl.getUniformLocation(glProgramData.program, name);
            //glProgramData.gl.uniform1i(imageLocation, index);
        }
    }
    return texture;
}


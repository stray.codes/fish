/**
* Creates and compiles a shader.
*
* @param {!WebGLRenderingContext} gl The WebGL Context.
* @param {string} shaderSource The GLSL source code for the shader.
* @param {number} shaderType The type of shader, VERTEX_SHADER or
*     FRAGMENT_SHADER.
* @return {!WebGLShader} The shader.
*/
function compileShader(gl, shaderSource, shaderType) {
    // Create the shader object
    var shader = gl.createShader(shaderType);
    // Set the shader source code.
    gl.shaderSource(shader, shaderSource);
    // Compile the shader
    gl.compileShader(shader);
    // Check if it compiled
    var success = gl.getShaderParameter(shader, gl.COMPILE_STATUS);
    if (!success) {
        // Something went wrong during compilation; get the error
        throw ("could not compile shader:" + gl.getShaderInfoLog(shader));
    }
    return shader;
}

/**
* Creates a program from 2 shaders.
*
* @param {!WebGLRenderingContext) gl The WebGL context.
* @param {!WebGLShader} vertexShader A vertex shader.
* @param {!WebGLShader} fragmentShader A fragment shader.
* @return {!WebGLProgram} A program.
*/
function createProgram(gl, vertexShader, fragmentShader) {
    // create a program.
    var program = gl.createProgram();
    // attach the shaders.
    gl.attachShader(program, vertexShader);
    gl.attachShader(program, fragmentShader);
    // link the program.
    gl.linkProgram(program);
    // Check if it linked.
    var success = gl.getProgramParameter(program, gl.LINK_STATUS);
    if (!success) {
        // something went wrong with the link; get the error
        throw ("program failed to link:" + gl.getProgramInfoLog(program));
    }
    return program;
}

function createGLProgram(gl, vertexShaderSource, fragmentShaderSource) {
    return createProgram(gl,
        compileShader(gl, vertexShaderSource, gl.VERTEX_SHADER),
        compileShader(gl, fragmentShaderSource, gl.FRAGMENT_SHADER));
}

export function GLProgramData(gl, vertexShaderSource, fragmentShaderSource) {
    this.gl = gl;
    this.program = createGLProgram(gl, vertexShaderSource, fragmentShaderSource);
    this.vao = gl.createVertexArray();

    this.use = function () {
        gl.useProgram(this.program);
        gl.bindVertexArray(this.vao);
        gl.enable(gl.BLEND);
        gl.blendFunc(gl.SRC_ALPHA, gl.ONE);
    }

    this.data = function () {
        return {
            gl: this.gl,
            program: this.program,
            vao: this.vao
        };
    }
}


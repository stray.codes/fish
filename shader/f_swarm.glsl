#version 300 es
// fragment shaders don't have a default precision so we need
// to pick one. highp is a good default. It means "high precision"
precision highp float;
precision highp int;


flat in ivec4 v_color;
// we need to declare an output for the fragment shader
out ivec4 outColor;

void main() {
  outColor = v_color;
  //outColor = vec4(1.0, 0, 1.0, 1.0);
}

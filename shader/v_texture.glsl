#version 300 es

in vec3 a_position;

float lod = 10000.0;

uniform vec2 u_resolution;
uniform int u_size;
uniform float u_time;
uniform float u_delta;
uniform int u_fishID;


vec2 u_translation = vec2(0.0);
vec2 u_velocity = vec2(0.0);
vec2 u_acceleration = vec2(0.0);
float u_accelerationS = 0.0;
float u_accelerationD = 0.0;
float u_accelerationT = 0.0;
uniform float u_maxAcceleration;
uniform float u_maxAccelerationDiff;
float u_rotation = 0.0;
float u_tilt = 0.0;
float PI = 3.141592653589793;

uniform highp isampler2D u_dataTexture;

out vec2 v_resolution;

mat2 rotationMatrix(float angle){
	return mat2(cos(angle), -sin(angle),
		    sin(angle),  cos(angle));
}

out vec2 v_texCoords;

vec4 getPixelF(int px, int id){
    return vec4(texelFetch(u_dataTexture, ivec2(px,id),0))/lod;
}
ivec4 getPixelI(int px, int id){
    return ivec4(texelFetch(u_dataTexture, ivec2(px,id),0));
}

// all shaders have a main function
void main() {
    v_resolution = u_resolution;

    //calculate information
    u_translation = getPixelF(0,u_fishID).xy;
    u_velocity = getPixelF(0,u_fishID).zw;
    u_acceleration = getPixelF(1,u_fishID).xy;
    u_accelerationD = getPixelF(3,u_fishID).y;
    u_accelerationS = getPixelF(1,u_fishID).w;
    u_accelerationT = getPixelF(3,u_fishID).x;

    u_rotation = asin(normalize(u_velocity).x);
    if(getPixelF(0,u_fishID).w<0.0)
        u_rotation = -u_rotation+PI;
    
    u_tilt = sin(u_accelerationT)*u_accelerationS+u_accelerationD;



    //calculate graphics
    v_texCoords = a_position.xy * vec2(1.0, 0.5);
    v_texCoords.y = 1.0 - v_texCoords.y; 

    vec2 position = a_position.xy - vec2(0.5, 1.25);

    float tilt = position.y;  
    if(tilt > 0.25)
        tilt = (tilt-0.25)*0.5;
    else if(tilt >= 0.0)
        tilt = 0.0;
    tilt = tilt * u_tilt;
    position = rotationMatrix(tilt) * position;
    position = rotationMatrix(u_tilt*0.1) * position;

    position = rotationMatrix(u_rotation) * position;
    position = position / (u_resolution/2.0);
    position = position * float(u_size);
    position = position + (u_translation*2.0)/u_resolution;


gl_Position = vec4(position, float(u_fishID)/1028.0, 1);
}



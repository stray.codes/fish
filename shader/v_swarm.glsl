#version 300 es

precision highp float;
precision highp int;

float lod = 10000.0;

// DATA STORED IN THE TEXTURE
// 0px.xy position
// 0px.ba velocity
// 1px.xy acceleration
// 1px.z accelerationD
// 1px.w accelerationS
// 2px.xy spawn
// 2px.ba food
// 3px.x accelerationT
// 3px.y accelerationD2
// 3px.zw previous velocity
// 4px.x foodTimer
// 4px.y state
// 4px.z previous resolution height
// 4px.w RESERVE
// 5px RESERVE

uniform highp isampler2D u_dataTexture;
uniform vec2 u_resolution;
uniform vec2 u_mouse;
uniform float u_delta;
float delta=0.0;
float PI = 3.141592653589793;

flat out ivec4 v_color;

uniform int u_amount;
uniform int u_nextAmount;
int textureWidth = 6;

uniform float u_fishUnit;
uniform float u_radiusFishSeparation;
uniform float u_radiusFishCohesion;
uniform float u_radiusFishAllignment;
uniform float u_radiusMouse1;

uniform float u_friction;
uniform float u_maxVelocity;
uniform float u_maxAcceleration;
uniform float u_maxAccelerationDiff;

uniform float u_scalarMouse;
uniform float u_scalarSeparation;
uniform float u_scalarAllignment;
uniform float u_scalarCohesion;
uniform float u_scalarFood;

uniform int u_mouseMode;

uniform int u_init;

int dataID = 0;
int fishID = 0;

vec2 position = vec2(0);
vec2 velocity = vec2(0);
vec2 acceleration = vec2(0);
float accelerationS = 0.0;
float accelerationT = 0.0;
float accelerationD = 0.0;
float state = 0.0;
vec2 spawn = vec2(0);
vec2 food = vec2(0);
float time0 = 0.0;
float time1 = 0.0;

vec2 mousePosition;



float random(vec2 seed)
{
    return fract(sin(dot(seed,vec2(12.9898,78.233)))*43758.5453123);
}

vec2 randomPos(int seed)
{
    return vec2(random(vec2(fishID+69+seed,fishID+42+seed))*u_resolution.x-u_resolution.x/2.0,random(vec2(fishID+3141+seed,fishID+2718+seed))*u_resolution.y-u_resolution.y/2.0);
}


vec2 getOutputPosition() {
    return vec2( 
            (float(dataID)/float(textureWidth)+1.0/(float(textureWidth)*2.0))*2.0-1.0, 
            (float(fishID)/float(u_amount)+1.0/(float(u_amount)*2.0))*2.0-1.0);
}

vec4 getPixelF(int px, int id){
    return vec4(texelFetch(u_dataTexture, ivec2(px,id),0))/lod;
}
ivec4 getPixelI(int px, int id){
    return ivec4(texelFetch(u_dataTexture, ivec2(px,id),0));
}

void getData(){
    vec4 temp;

    temp = getPixelF(0,fishID);
    position = temp.xy;
    velocity = temp.zw;

    acceleration = getPixelF(1,fishID).xy;

    temp = getPixelF(2,fishID);
    spawn = temp.xy;
    food = temp.zw;

    temp = getPixelF(4,fishID);
    state = temp.y;

}

void main() {
    dataID = int(gl_VertexID) % textureWidth ;
    fishID = int(gl_VertexID) / textureWidth;
    vec4 out_data=getPixelF(dataID,fishID);
    delta = u_delta;
    if(delta > 1.0)
        delta=1.0;
    getData();


    mousePosition = vec2(u_mouse.x,u_resolution.y-u_mouse.y)-u_resolution/2.0;
    vec4 frame =
        vec4(float(u_resolution.y)/2.0,float(u_resolution.x)/2.0,-float(u_resolution.y)/2.0,-float(u_resolution.x)/2.0);


    if(u_init==0 && (state==0.0 || state == 1.0 || state == 2.0))
    {
        if(dataID==0)
        {
            //velocity calculation
            velocity = velocity*(1.0-u_friction*delta)+acceleration*delta;
            if(length(velocity) < 100.0)
                velocity = normalize(velocity)*100.0;
            if(length(velocity) > u_maxVelocity)
                velocity = normalize(velocity)*u_maxVelocity;
            out_data.zw = velocity;

            //adapt to moving resolution
            if(getPixelI(4,fishID).z != int(u_resolution.y))
                position.y = position.y - float(getPixelI(4,fishID).z - int(u_resolution.y))/2.0;

            position = position+velocity*delta;

            //portal effect
            if(state!=1.0){
                if(position.x<frame.w-3.0*u_fishUnit)
                    position.x = frame.y+2.5*u_fishUnit;
                if(position.x>frame.y+3.0*u_fishUnit)
                    position.x = frame.w-2.5*u_fishUnit;
                if(position.y<frame.z-3.0*u_fishUnit)
                    position.y = frame.x+2.5*u_fishUnit;
                if(position.y>frame.x+3.0*u_fishUnit)
                    position.y = frame.z-2.5*u_fishUnit;
            }
            if(state==1.0)
            {
                if((position.x<frame.w-3.0*u_fishUnit) || (position.x>frame.y+3.0*u_fishUnit) ||
                        (position.y<frame.z-3.0*u_fishUnit) || (position.y>frame.x+3.0*u_fishUnit)
                        || fishID<u_nextAmount )
                {
                    vec2 randomData = randomPos(3);
                    position = normalize(randomData.xy)*(u_resolution.x+u_resolution.y);
                }
            }

            out_data.xy = position;
        }
        else if(dataID==1)
        {
            vec2 accelerationOld = acceleration;

            vec2 mouseDirection = vec2(0);
            float mouseDistance=distance(position,mousePosition);
            float scalarMouse = u_scalarMouse;
                
            if(mouseDistance<u_radiusMouse1*u_fishUnit)
                mouseDirection = -normalize(position-mousePosition);
            if(u_mouseMode==1){
                if(mouseDistance<u_radiusMouse1*u_fishUnit*1.0){
                    mouseDirection = -normalize(position-mousePosition);
                }
            }
            if(u_mouseMode == 1){
                scalarMouse=scalarMouse*-0.3;
                if(mouseDistance<u_radiusMouse1*u_fishUnit*0.5)
                    scalarMouse=0.0;


            }

            vec2 foodDirection = food-position;


            vec2 separationDirection = vec2(0);
            vec2 cohesionDirection = vec2(0);
            vec2 allignmentDirection = vec2(0);
            vec2 tempPosition = vec2(0);
            vec2 tempVelocity = vec2(0);

            for(int i=0; i<u_amount; i++)
            {
                if(fishID != i)
                {
                    tempPosition=getPixelF(0,i).xy;
                    tempVelocity=getPixelF(0,i).zw;
                    if(distance(tempPosition,position)<u_radiusFishSeparation*u_fishUnit) {
                        separationDirection=separationDirection+(tempPosition-position);
                    }
                    if(distance(tempPosition,position)<u_radiusFishCohesion*u_fishUnit){
                        cohesionDirection=cohesionDirection+(tempPosition-position);
                    }
                    if(distance(tempPosition,position)<u_radiusFishAllignment*u_fishUnit){
                        allignmentDirection=allignmentDirection+(tempVelocity);
                    }
                }
            }
            if(length(separationDirection)>0.0)
                separationDirection = normalize(separationDirection);
            if(length(cohesionDirection)>0.0)
                cohesionDirection = normalize(cohesionDirection);
            if(length(allignmentDirection)>0.0)
                allignmentDirection = normalize(allignmentDirection);
            if(length(foodDirection)>0.0)
                foodDirection = normalize(foodDirection);

            if(state==0.0)
            {
                acceleration = mouseDirection*scalarMouse
                    + separationDirection*u_scalarSeparation 
                    + cohesionDirection*u_scalarCohesion 
                    + allignmentDirection*u_scalarAllignment;
            }
            if(state==1.0)
            {
                acceleration = mouseDirection*scalarMouse
                    + separationDirection*u_scalarSeparation
                    + normalize(spawn)*u_scalarFood*2.0;
            }
            if(state==2.0)
            {
                acceleration = mouseDirection*scalarMouse
                    + separationDirection*u_scalarSeparation
                    + foodDirection*u_scalarFood;
            }


            vec2 accelerationDiff = (acceleration - accelerationOld);
            if(length(accelerationDiff)>u_maxAccelerationDiff)
                accelerationDiff = normalize(accelerationDiff)*u_maxAccelerationDiff*delta;
            acceleration=accelerationOld+accelerationDiff;

            if(length(acceleration)>u_maxAcceleration)
                acceleration=normalize(acceleration)*u_maxAcceleration;

            out_data.xy = acceleration;




            float oldAccelerationD = getPixelF(1,fishID).z;
            accelerationD = 0.0;
            vec2 oldVelocity = getPixelF(3,fishID).zw;

            if(length(oldVelocity)>0.0 && length(velocity)>0.0)
                accelerationD = acos(dot(oldVelocity,velocity)/(length(oldVelocity)*length(velocity)));
            float tiltDirection = oldVelocity.x*velocity.y - oldVelocity.y*velocity.x;
            if(tiltDirection>0.0)
                accelerationD=accelerationD*-1.0;

            accelerationD = oldAccelerationD + accelerationD*delta;

            if(accelerationD>PI)
                accelerationD=PI;
            if(accelerationD<-PI)
                accelerationD=-PI;

            accelerationD=accelerationD*(1.0-(length(velocity)/u_maxVelocity)*delta);
            accelerationD= oldAccelerationD - sign(oldAccelerationD-accelerationD)*9.0*delta;
            if(oldAccelerationD>0.0 && accelerationD<0.0)
                accelerationD=0.0;
            if(oldAccelerationD<0.0 && accelerationD>0.0)
                accelerationD=0.0;


            if((position.x<frame.w-3.0*u_fishUnit) || (position.x>frame.y+3.0*u_fishUnit) ||
                        (position.y<frame.z-3.0*u_fishUnit) || (position.y>frame.x+3.0*u_fishUnit)
                        || fishID<u_nextAmount )
            {
                accelerationD=0.0;
            }

            out_data.z = accelerationD;




            float oldAccelerationS = getPixelF(1,fishID).w;
            accelerationS=length(acceleration)*0.8;
            accelerationS = accelerationS/u_maxAcceleration;
            accelerationS = oldAccelerationS + sign(accelerationS-oldAccelerationS)*delta;
            if(accelerationS*sign(accelerationS)<1.0*delta)
                accelerationS=0.0;
            out_data.w = accelerationS;
        }
        else if(dataID==2)
        {
            out_data.xy = spawn;
            if(length(spawn)==0.0){
                vec2 randomData = randomPos(int(u_delta*1000.0)%255);
                if(length(randomData)==0.0)
                    randomData= vec2(1.0);
                out_data.xy = randomData.xy;
            }
            out_data.zw = food;
            if(distance(position,food)<u_fishUnit) 
                out_data.zw = randomPos(int(u_delta*1000.0)%255)*0.8;

            if((food.x<frame.w) || (food.x>frame.y) ||
                    (food.y<frame.z) || (food.y>frame.x))
            {
                out_data.zw = randomPos(int(u_delta*1000.0)%255)*0.8;
            }
        }
        else if(dataID==3)
        {
            accelerationT = out_data.x;
            accelerationT=accelerationT+delta*(length(velocity)/u_maxVelocity)*2.0*PI*9.0;
            //accelerationT = 0.0; //TODO remove me
            out_data.x = accelerationT;
//POTAT
            accelerationD = getPixelF(1,fishID).z;
            float accelerationD2 = getPixelF(3,fishID).y;
            if(accelerationD*sign(accelerationD)<0.2)
                accelerationD=0.0;
            float accelerationDDiff = accelerationD-accelerationD2;
            float speed = 2.5*delta;
            if(accelerationDDiff*sign(accelerationDDiff) <= speed)
                accelerationD2 = accelerationD;
            else 
                accelerationD2 = accelerationD2 + sign(accelerationDDiff)*speed;

            out_data.y = accelerationD2;

            out_data.zw = velocity;
        }
        else if(dataID==4)
        {
            float modeChangeTimer = getPixelF(4,fishID).x;

            if(state!=1.0)
            {
                modeChangeTimer=modeChangeTimer-delta;

                if(modeChangeTimer<=0.0)
                    state=2.0;
                if(distance(position,food)<u_fishUnit && state == 2.0)
                {
                    modeChangeTimer=random(vec2(fishID,3))*60.0+5.0;
                    state=0.0;
                }
                if(distance(position,mousePosition)<u_radiusMouse1*u_fishUnit && state == 2.0)
                {
                    modeChangeTimer=random(vec2(fishID,3))*60.0+5.0;
                    state=0.0;
                }
                if(fishID>=u_nextAmount)
                    state=1.0;
            }
            else {
                if(fishID<u_nextAmount)
                    state=0.0;
            }

            out_data.x = modeChangeTimer;
            out_data.y = state;
            out_data.z = float(u_resolution.y)/lod;

        }
    }
      
    if(u_init==1)
    {
        out_data = vec4(0);
        vec2 randomData;
        if(dataID==0)
        {
            randomData = randomPos(3);
            if(length(randomData)==0.0)
                randomData= vec2(1.0);
            out_data.xy = normalize(randomData.xy)*(u_resolution.x+u_resolution.y);
            randomData = randomPos(2);
            if(length(randomData)==0.0)
                randomData= vec2(1.0);
            out_data.zw = randomData.xy;
        }
        if(dataID==2)
        {
            //random spawn pos
            randomData = randomPos(3);
            if(length(randomData)==0.0)
                randomData= vec2(1.0);
            out_data.xy = randomData.xy;
            //random food pos
            randomData = randomPos(4);
            out_data.zw = randomData.xy*0.8;
        }
        if(dataID==4)
        {
            //mode change timer
            out_data.x=random(vec2(fishID,543))*60.0+5.0;
            //initial state
            out_data.y=1.0;
            out_data.z = float(u_resolution.y)/lod;
        }
    }

    v_color = ivec4(out_data*lod);
    gl_PointSize = 1.0;
    vec2 outputPos = getOutputPosition();
    gl_Position = vec4(outputPos, float(fishID)/1000.0, 1.0);
}



#version 300 es
// fragment shaders don't have a default precision so we need
// to pick one. highp is a good default. It means "high precision"
precision highp float;

// we need to declare an output for the fragment shader
out vec4 outColor;

in vec2 v_texCoords;


void main() {
  outColor = vec4(v_texCoords.x, 0.5, v_texCoords.y, 1);
  //outColor = vec4(0.0, 0.0, 0.0, 1);
}

#version 300 es
// fragment shaders don't have a default precision so we need
// to pick one. highp is a good default. It means "high precision"
precision highp float;

uniform vec3 u_color0;
uniform vec3 u_color1;
uniform vec3 u_color2;
uniform vec3 u_color3;
uniform vec3 u_color4;
uniform vec3 u_color5;

// we need to declare an output for the fragment shader
out vec4 outColor;

in vec2 v_texCoords;

uniform sampler2D u_texture;


vec3 color0 = vec3(1,0,0);
vec3 color1 = vec3(0,1,0);
vec3 color2 = vec3(0,0,1);
vec3 color3 = vec3(1,1,0);
vec3 color4 = vec3(0,1,1);
vec3 color5 = vec3(1,0,1);
float tolerance = 0.3;

void main() {
    vec4 textureColor =texture(u_texture, v_texCoords);
    if(textureColor.a == 0.0)
        discard;
    else
    { 
        outColor = textureColor;
        //if(distance(textureColor.rgb, color0.rgb)<0.4)
            //outColor.rgb = u_color0;
        if(distance(textureColor.rgb, color0.rgb)<=tolerance)
            outColor.rgb = u_color0;
        else if(distance(textureColor.rgb, color1.rgb)<=tolerance)
            outColor.rgb = u_color1;
        else if(distance(textureColor.rgb, color2.rgb)<=tolerance)
            outColor.rgb = u_color2;
        else if(distance(textureColor.rgb, color3.rgb)<=tolerance)
            outColor.rgb = u_color3;
        else if(distance(textureColor.rgb, color4.rgb)<=tolerance)
            outColor.rgb = u_color4;
        else if(distance(textureColor.rgb, color5.rgb)<=tolerance)
            outColor.rgb = u_color5;
        else 
            outColor.rgb = u_color0;
    }
    
}
